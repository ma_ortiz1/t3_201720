package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	public void addFirst(T t);
	public void addAtEnd(T t);
	public void addAdK(T t, int pos);
	public T getElement(int pos);
	public T getCurrentElement();
	public Integer getSize();
	public void deleteFirst();
	public void deleteLast();
	public void deleteAtK(int pos);
	public T next();
	public T previous();
	
	
}
