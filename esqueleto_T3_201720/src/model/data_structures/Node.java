package model.data_structures;

public class Node<T>
{
	private T data;
	private Node<T> prev; 
	private Node<T> next;
	
	public Node( T d, Node<T> p, Node<T> n )
	{
		data = d;
		prev = p;
		next = n; 
		
	}
	
	public T getData(){
		return data;
	}
	
	public Node<T> getPrev(){
		return prev;
	}
	
	public Node<T> getNext(){
		return next;
	}
	
	public void changeData(T newData) {
		data = newData;
	}
	
	public void changePrev(Node<T> newNode) {
		prev = newNode;
	}
	
	public void changeNext(Node<T> newNode) {
		next = newNode;
	}
	
} 


