package model.logic.test;
import static org.junit.Assert.*;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.logic.STSManager;
import model.vo.BusUpdateVO;
import model.vo.StopVO;



public class STSManagerTest extends TestCase{

	private STSManager manager;

	public void setUpEscenario1(){
		manager=new STSManager();
	}

	public void setUpEscenario2()
	{
		manager=new STSManager();
		manager.loadStops();


	}

	public void setUpEscenario3()
	{
		manager=new STSManager();
		manager.iniciarList();
		File f = new File("data");
		File[] updateFiles = f.listFiles();


		String[] split=updateFiles[0].getName().split("\\.");
		String ext = split[split.length - 1];

		if(ext.equals("json")){
			manager.readBusUpdate(updateFiles[0]);
		}
	}

	public void setUpEscenario4()
	{
		manager=new STSManager();
		manager.iniciarList();
		File f = new File("data");
		File[] updateFiles = f.listFiles();

		for (int i=0; i<4;i++){

			String[] split=updateFiles[i].getName().split("\\.");
			String ext = split[split.length - 1];

			if(ext.equals("json")){
				manager.readBusUpdate(updateFiles[i]);
			}
		}
	}

	public void testLoadStops()
	{
		try{	
			setUpEscenario1();
			manager.loadStops();
		}
		catch(Exception e){

			fail ("No se cargaron correctamente los archivos");
		}
	}

	public void testListStops()
	{
		setUpEscenario2();
		StopVO ac=null;

		IList<StopVO>  listStop=manager.getListStop();	
		Iterator<StopVO> inter=listStop.iterator();
		while(inter.hasNext()){
			ac=inter.next();
			if(ac.getStopId().equals("12034")){
				break;
			}

		}
		assertEquals("12034", ac.getStopId());

	}
	public void testListVoStop()
	{
		setUpEscenario2();



		IList<StopVO>  listStop=manager.getListStop();	
		StopVO ac=listStop.getElement(listStop.getSize()-1);
		assertEquals("La parada no tiene el stopId esperado: 12034","12034",ac.getStopId());
		assertEquals("La parada no tiene el nombre esperado","WATERFRONT STATION ALL LINES",ac.getStopName());
		
	}

	public void testjson(){
		setUpEscenario3();
		IQueue<BusUpdateVO> busStop=manager.getQueueBuss();
		BusUpdateVO ac=busStop.first();
		assertEquals("El primero de la lista deber�a de tener el tripId: 9135648", "9135648",ac.getTripId());


	}

	public void testjson2(){
		setUpEscenario4();
		IQueue<BusUpdateVO> busStop=manager.getQueueBuss();
		BusUpdateVO ac=busStop.first();
		int size=busStop.getSize().intValue();
		assertFalse("El tama�o de la lista no deber�a ser cero",size==0);
		assertEquals("El primero de la lista deber�a de tener el tripId: 9135648", "9135648",ac.getTripId());
		ac=busStop.dequeue();
		assertEquals("9135648", ac.getTripId());
		
		

	}



}
