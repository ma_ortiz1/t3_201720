package model.data_structures.test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.*;

public class DoubleLinkedListTest<T> extends TestCase{
	
	private DoubleLinkedList<T> listaDoble;
	
	public void setupEscenario1()
	{
		listaDoble = new DoubleLinkedList<>();
	}
	
	public void setupEscenario2()
	{
		listaDoble = new DoubleLinkedList<>();
		Integer num1 = new Integer(50);
		Integer num2 = new Integer(40);
		Integer num3 = new Integer(30);
		Integer num4 = new Integer(20);
		Integer num5 = new Integer(10);
		listaDoble.add((T) num1);
		listaDoble.add((T) num2);
		listaDoble.add((T) num3);
		listaDoble.add((T) num4);
		listaDoble.add((T) num5);
	}
	
	public void testIsEmpty()
	{
		setupEscenario1();
		assertEquals("La lista deber�a estar vac�a", true, listaDoble.isEmpty());
		setupEscenario2();
		assertEquals("La lista no deber�a estar vacia", false, listaDoble.isEmpty());
	}
	
	public void testGetSize1()
	{
		setupEscenario1();
		Integer size = listaDoble.getSize();
		assertEquals("Error, el tamaño deberìa ser 0", 0, size.intValue());
	}
	
	public void testGetSize2()
	{
		setupEscenario2();
		Integer size = listaDoble.getSize();
		assertEquals("Error, el tamaño deberia ser 5", 5, size.intValue());
	}
	
	public void testAdd1()
	{
		setupEscenario1();
		Integer num1 = new Integer(1);
		listaDoble.add((T) num1);
		Integer size = listaDoble.getSize();
		assertEquals("Error, el tamaño deberìa ser 1", 1, size.intValue());
		Integer num2 = new Integer(1);
		listaDoble.add((T) num2);
		size = listaDoble.getSize();
		assertEquals("Error, el tamaño deberìa ser 2", 2, size.intValue());
	}
	
	public void testFirst()
	{
		setupEscenario2();
		Integer esperado = new Integer(50);
		T actual = listaDoble.first();
		assertEquals("El primero obtenido no es el correcto", esperado, actual);
	}
	
	public void testLast()
	{
		setupEscenario2();
		Integer esperado = new Integer(10);
		T actual = listaDoble.last();
		assertEquals("El ultimo obtenido no es el correcto", esperado, actual);
	}
	
	public void testAddFirst()
	{
		setupEscenario2();
		Integer nuevo = new Integer (45);
		listaDoble.addFirst((T) nuevo);
		T actual = listaDoble.first();
		assertEquals("El dato añadido no està en la primera posiciòn", nuevo, actual);
	}
	public void testAddAtEnd()
	{
		setupEscenario2();
		Integer nuevo = new Integer (45);
		listaDoble.addAtEnd((T) nuevo);
		T actual = listaDoble.last();
		assertEquals("El dato añadido no està en la ultima posiciòn", nuevo, actual);
	}
	
	public void testDeleteFirst()
	{
		setupEscenario2();
		T primero = listaDoble.first();
		listaDoble.deleteFirst();
		T nuevoPrimero = listaDoble.first();
		assertNotSame("El primer elemento sigue siendo el mismo", primero, nuevoPrimero);
	}
	
	public void testDeleteLast()
	{
		setupEscenario2();
		T ultimo = listaDoble.last();
		listaDoble.deleteLast();
		T nuevoUltimo = listaDoble.last();
		assertNotSame("El ultimo elemento sigue siendo el mismo", ultimo, nuevoUltimo);
	}
	
	public void testAddAtK()
	{
		setupEscenario2();
		Integer nuevo = new Integer(222);
		listaDoble.addAdK((T) nuevo, 3);
		T pos3 = listaDoble.getElement(3);
		assertEquals("El elemeno en esa posiciòn no es el correcto", nuevo, pos3);
	}
	
	public void testIterator()
	{
		setupEscenario2();
		Iterator iter = listaDoble.iterator();
		int num = 0;
		while(iter.hasNext())
		{
			Integer actual = (Integer) iter.next();
			num++;
		}
		assertEquals("El iterador no concuerda con el tamaño", num, listaDoble.getSize().intValue());
	}
}
