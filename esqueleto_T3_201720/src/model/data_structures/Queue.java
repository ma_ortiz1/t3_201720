package model.data_structures;
public class Queue<E> implements IQueue<E> {
	
	private Node<E> first;
	private Node<E> last;
	private int size;
	
	private class Node<E>
	{
		E e;
		Node<E> next;
	}
	public Queue() {
		size = 0;
	}
	

	@Override
	public void enqueue(E e) {
		// TODO Auto-generated method stub
		Node<E> oldLast = last;
		last = new Node<E>();
		last.e = e;
		last.next = null;
		if(isEmpty())
		{
			first = last;
		}
		else
		{
			oldLast.next = last;
		}
		size++;
		
	}

	@Override
	public E dequeue() {
		// TODO Auto-generated method stub
		E e = first.e;
		first = first.next;
		if(isEmpty())
		{
			last = null;
		}
		size --;
		return e;
		
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return new Integer(size);
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		return first != null? first.e:null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}

}
