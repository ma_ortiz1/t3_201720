package model.data_structures.test;
import junit.framework.TestCase;
import model.data_structures.*;

public class QueueTest<T> extends TestCase{

	private Queue<T> queue;
	
	public void setupEscenario1()
	{
		queue = new Queue<>();
	}
	
	public void setupEscenario2()
	{
		queue = new Queue<>();
		Integer num1 = new Integer(10);
		Integer num2 = new Integer(20);
		Integer num3 = new Integer(30);
		Integer num4 = new Integer(40);
		Integer num5 = new Integer(50);
		queue.enqueue((T)num1);
		queue.enqueue((T)num2);
		queue.enqueue((T)num3);
		queue.enqueue((T)num4);
		queue.enqueue((T)num5);
	}
	
	public void testIsEmpty()
	{
		setupEscenario1();
		assertEquals("No deberìa haber datos", true, queue.isEmpty());
		setupEscenario2();
		assertEquals("Deberìa haber elementos", false, queue.isEmpty());
	}
	
	public void testGetSize()
	{
		setupEscenario1();
		assertEquals("El nùmero de elementos deberìa ser 0", 0, queue.getSize().intValue());
		setupEscenario2();
		assertEquals("El numero de elementos deberìa ser 5", 5, queue.getSize().intValue());
	}
	
	public void testfirst()
	{
		Integer num1 = new Integer(100);
		setupEscenario2();
		assertEquals("El primer elemento no es el correcto", 10, queue.first());
		queue.enqueue((T) num1);
		assertEquals("El primer elemento no deberìa cambiar", 10, queue.first());
		
	}
	
	public void testEnqueue()
	{
		setupEscenario1();
		Integer num1 = new Integer(200);
		Integer num2= new Integer(300);
		assertEquals("Deberìa ser 0", 0, queue.getSize().intValue());
		queue.enqueue((T) num1);
		assertEquals("Deberìa ser 1", 1, queue.getSize().intValue());
		queue.enqueue((T) num2);
		assertEquals("Deberìa ser 2", 2, queue.getSize().intValue());
		
		setupEscenario2();
		assertEquals("Deberìa ser 5", 5, queue.getSize().intValue());
		queue.enqueue((T) num1);
		assertEquals("Deberìa ser 6", 6, queue.getSize().intValue());
		queue.enqueue((T) num2);
		assertEquals("Deberìa ser 7", 7, queue.getSize().intValue());
		
	}
	
	public void testDequeue()
	{
		setupEscenario1();
		Integer num1 = new Integer(10);
		Integer num2 = new Integer(20);
		Integer num3 = new Integer(30);
		Integer num4 = new Integer(40);
		Integer num5 = new Integer(50);
		queue.enqueue((T) num1);
		queue.enqueue((T) num2);
		queue.enqueue((T) num3);
		queue.enqueue((T) num4);
		queue.enqueue((T) num5);
		
		T del1= queue.dequeue();
		assertEquals("El dato eliminado no es el adecuado", num1, del1);
		assertEquals("Deberìa haber menos elementos", 4, queue.getSize().intValue());
		
		del1= queue.dequeue();
		assertEquals("El dato eliminado no es el adecuado", num2, del1);
		assertEquals("Deberìa haber menos elementos", 3, queue.getSize().intValue());
	}
}
