package model.vo;

public class BusUpdateVO {
	private String VehicleNo;
	private String TripId;
	private String RouteNo;
	private String Destination;
	private String Pattern;
	private Double Latitude;
	private Double Longitude;
	private String RecordedTime;
	private RMap RouteMap;
	public class RMap{
		private String Href;
		public RMap(String Href){
			this.setHref(Href);
		}
		/**
		 * @return the href
		 */
		public String getHref() {
			return Href;
		}
		/**
		 * @param href the href to set
		 */
		public void setHref(String href) {
			Href = href;
		}
	}

	public BusUpdateVO(String VehicleNo, String TripId,String RouteNo, String Destination, String Pattern, Double Latitude, Double Longitude, String RecordedTime,RMap RouteMap )
	{
		this.VehicleNo=VehicleNo;
		this.TripId=TripId;
		this.VehicleNo=VehicleNo;
		this.Destination=Destination;
		this.Pattern=Pattern;
		this.Latitude=Latitude;
		this.Longitude=Longitude;
		this.RecordedTime=RecordedTime;
		this.RouteMap=new RMap(RouteMap.getHref());

	}
	/**
	 * @return the vehicleNo
	 */
	public String getVehicleNo() {
		return VehicleNo;
	}
	/**
	 * @param vehicleNo the vehicleNo to set
	 */
	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}
	/**
	 * @return the tripId
	 */
	public String getTripId() {
		return TripId;
	}
	/**
	 * @param tripId the tripId to set
	 */
	public void setTripId(String tripId) {
		this.TripId = tripId;
	}
	/**
	 * @return the routeNo
	 */
	public String getRouteNo() {
		return RouteNo;
	}
	/**
	 * @param routeNo the routeNo to set
	 */
	public void setRouteNo(String routeNo) {
		RouteNo = routeNo;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return Destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		Destination = destination;
	}
	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return Pattern;
	}
	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		Pattern = pattern;
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return Latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		Latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return Longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		Longitude = longitude;
	}
	/**
	 * @return the recordedTime
	 */
	public String getRecordedTime() {
		return RecordedTime;
	}
	/**
	 * @param recordedTime the recordedTime to set
	 */
	public void setRecordedTime(String recordedTime) {
		RecordedTime = recordedTime;
	}
	/**
	 * @return the routeMap
	 */
	public RMap getRouteMap() {
		return RouteMap;
	}
	/**
	 * @param routeMap the routeMap to set
	 */
	public void setRouteMap(RMap routeMap) {
		RouteMap = routeMap;
	}


}
