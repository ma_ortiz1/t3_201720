package controller;

import java.io.File;

import model.data_structures.IStack;
import model.exceptions.TripNotFoundException;
import model.logic.STSManager;
import model.vo.StopVO;
import api.ISTSManager;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadStops() {
		manager.loadStops();		
	}

	public static void readBusUpdates() {
		manager.iniciarList();
		File f = new File("data");
		File[] updateFiles = f.listFiles();

		for (int i = 0; i < updateFiles.length; i++) {
			String[] split=updateFiles[i].getName().split("\\.");
			String ext = split[split.length - 1];

			if(ext.equals("json")){
				manager.readBusUpdate(updateFiles[i]);

			}

		}
	}


	public static void listStops(Integer tripId) throws TripNotFoundException{

		IStack<StopVO> list=manager.listStops(tripId);
		if(list.getSize()==0 || list.isEmpty() || list==null){
			throw new TripNotFoundException();
		}	
		else{
			int size=list.getSize();
			
			while(size>0){
				StopVO ac=list.pop();
				System.out.println(ac.getStopName());
				
				size=list.getSize();
			}
		}
	
	}


}
