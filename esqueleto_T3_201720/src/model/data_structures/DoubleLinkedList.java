package model.data_structures;

import java.util.Iterator;

import model.data_structures.Node;

public class DoubleLinkedList<T> implements IList<T> {

	private Node<T> head;
	private Node<T> tail;
	private int size;

	public DoubleLinkedList()
	{
		clear();
	}
	
	public void clear()
	{
		head = new Node<T>( null, null, null ); 
		tail = new Node<T>( null, head, null ); 
		head.changeNext(tail);
		size = 0; 
	}
	
	
	@Override
	public Iterator iterator() {
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator <T>{
		private Node<T> current = head.getNext();
		public boolean hasNext() {
			return current != tail;
		}
		public void remove() {
			DoubleLinkedList.this.remove( current.getPrev() );
		}
		public T next(){
			if( !hasNext() ) 
				throw new java.util.NoSuchElementException(); 
			
			T t= (T) current.getData();
			current = current.getNext();
			return t;
		}
	}
	
	public void add( T x )
	{
		add( size, x );
	}
	public void add( int idx, T x )
	{
		addBefore( getNode( idx, 0, size ), x );
	}
	private void addBefore( Node<T> p, T x )
	{
		Node<T> newNode = new Node<T>( x, p.getPrev(), p ); 
		newNode.getPrev().changeNext(newNode);
		p.changePrev(newNode); 
		size++ ;
	} 
	
	public T remove( int idx )
	{
		return remove( getNode( idx, 0, size ) );
	}
	
	private T remove( Node<T> p )
	{
		p.getNext().changePrev(p.getPrev());
		p.getPrev().changeNext(p.getNext());
		p.changeNext(null);
		p.changePrev(null); 
		size-- ;
		return p.getData();
	} 
	
	private Node<T> getNode( int idx, int lower, int upper )
	{
		Node<T> p;
		if ( idx < lower || idx > upper )
			throw new IndexOutOfBoundsException();
		if ( idx < size / 2 )
			{
				p = head.getNext();
				for ( int i = 0; i < idx; i++ )
				p = p.getNext(); 
			}
		else
			{
				p = tail;
				for ( int i = size; i > idx; i-- ) 
					p = p.getPrev();
			}
		return p;
	} 
	
	@Override
	public Integer getSize() {
		return new Integer(size);
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public T first() {
		Node<T> primero = getNode(0, 0, size);
		return primero.getData();
	}
	
	public T last() {
		Node<T> ultimo = getNode(size-1, 0, size);
		return ultimo.getData();
	}

	public void addFirst(T t)
	{
		add(0, t);
	}

	@Override
	public void addAtEnd(T t) {
		add(size, t);
	}

	@Override
	public void addAdK(T t, int pos) {
		add(pos, t);
	}

	@Override
	public T getElement(int pos) {
		Node<T> buscado = getNode(pos, 0, size);
		return buscado.getData();
	}

	@Override
	public void deleteFirst() {
		remove(0);
	}

	@Override
	public void deleteLast() {
		remove(size-1);
	}

	@Override
	public void deleteAtK(int pos) {
		remove(pos);
	}

	
	
	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public T next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return null;
	}

}
