package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import com.google.gson.Gson;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{
	private IList<StopVO> listVOStop;
	private IQueue <BusUpdateVO> colaBus;

	public void iniciarList(){
		colaBus=new Queue<BusUpdateVO>();
	}


	@Override
	public void loadStops() {
		// TODO Auto-generated method stub
		listVOStop=new DoubleLinkedList<StopVO>();

		File f= new File("data/stops.txt");
		FileReader reader; 
		try 
		{
			if (f.exists())
			{
				reader = new FileReader(f);
				BufferedReader lector = new BufferedReader(reader);
				String line = lector.readLine();
				line=lector.readLine();

				while(line != null )
				{


					String []stops = line.split(",");

					String stopId=stops[0];

					String stopName= stops[2];
					String stopDesc=stops[3];					
					double  stopLat=Double.parseDouble(stops[4]);
					double  stopLon=Double.parseDouble(stops[5]);
					String zoneId=stops[6];
					int locationType=Integer.parseInt(stops[8]);
					StopVO stop=new StopVO(stopId, stopName, stopDesc, stopLat, stopLon, zoneId, locationType);
					listVOStop.addAtEnd(stop);
					line=lector.readLine();		

				}
				lector.close();
				reader.close();


			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub

		Gson gson=new Gson();
		try {
			BusUpdateVO[] myt= gson.fromJson(new FileReader(rtFile), BusUpdateVO[].class);
			for(int i= 0; i< myt.length;i++){
				colaBus.enqueue(myt[i]);	

			}			
		}catch(Exception e){		
			e.printStackTrace();
		}
	}


	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}



	@Override
	public IStack<StopVO> listStops(Integer tripID){

		// TODO Auto-generated method stub
		IList<BusUpdateVO> listrip;
		IStack<StopVO> listStop;
		listStop =new Stack<StopVO>();
		listrip=new DoubleLinkedList<BusUpdateVO>();
		String id=Integer.toString(tripID);
		Integer size=colaBus.getSize();
		int i =0;
		int j=0;

		while(i<=size ){
			BusUpdateVO ac=colaBus.dequeue();

			if(ac.getTripId().equals(id)){
				listrip.addAtEnd(ac);
				j++;


			}
			i++;
			colaBus.enqueue(ac);

		}
		Iterator<BusUpdateVO> inter =listrip.iterator();

		while(inter.hasNext()){
			BusUpdateVO ac=inter.next();
			Iterator<StopVO> inS=listVOStop.iterator();
			while(inS.hasNext()){
				StopVO acs=inS.next();
				Double distance =getDistance(acs.getStopLat(), acs.getStopLon(), ac.getLatitude(), ac.getLongitude());
				if (Math.abs(distance) <= 70)
				{
					listStop.push(acs);
				}


			}
		}


		return listStop;
	}
	
	public IList<StopVO> getListStop() {
		return listVOStop;
		
	}
	public IQueue<BusUpdateVO> getQueueBuss(){
		return colaBus;
	}
}
