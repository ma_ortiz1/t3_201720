package model.vo;

/**
 * Representation of a Stop object
 */
public class StopVO {
	private String stopId;
	
	private String stopName;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneId;
	private int locationType;



	public StopVO (String stopId, String stopName,String stopDesc, double stopLat, double stopLon, String zoneId, int locationType)
	{
		this.stopId=stopId;
		
		this.stopName=stopName;
		this.stopDesc=stopDesc;
		this.stopLat=stopLat;
		this.stopLon=stopLon;
		this.zoneId=zoneId;
		this.locationType=locationType;
	}

	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the stopId
	 */
	public String getStopId() {
		return stopId;
	}

	/**
	 * @param stopId the stopId to set
	 */
	public void setStopId(String stopId) {
		this.stopId = stopId;
	}

	

	/**
	 * @return the stopName
	 */
	public String getStopName() {
		return stopName;
	}

	/**
	 * @param stopName the stopName to set
	 */
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	/**
	 * @return the stopDesc
	 */
	public String getStopDesc() {
		return stopDesc;
	}

	/**
	 * @param stopDesc the stopDesc to set
	 */
	public void setStopDesc(String stopDesc) {
		this.stopDesc = stopDesc;
	}

	/**
	 * @return the stopLat
	 */
	public double getStopLat() {
		return stopLat;
	}

	/**
	 * @param stopLat the stopLat to set
	 */
	public void setStopLat(double stopLat) {
		this.stopLat = stopLat;
	}

	/**
	 * @return the stopLon
	 */
	public double getStopLon() {
		return stopLon;
	}

	/**
	 * @param stopLon the stopLon to set
	 */
	public void setStopLon(double stopLon) {
		this.stopLon = stopLon;
	}

	/**
	 * @return the zoneId
	 */
	public String getZoneId() {
		return zoneId;
	}

	/**
	 * @param zoneId the zoneId to set
	 */
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	/**
	 * @return the locationType
	 */
	public int getLocationType() {
		return locationType;
	}

	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(int locationType) {
		this.locationType = locationType;
	}

}
