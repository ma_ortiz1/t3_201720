package model.data_structures.test;


import junit.framework.TestCase;
import model.data_structures.*;

public class StackTest<T> extends TestCase
{
	private Stack<T> stack;
	
	public void setupEscenario1()
	{
		stack = new Stack<>();
	}
	public void setupEscenario2()
	{
		stack = new Stack<>();
		Integer num1 = new Integer(10);
		Integer num2 = new Integer(20);
		Integer num3 = new Integer(30);
		Integer num4 = new Integer(40);
		Integer num5 = new Integer(50);
		stack.push((T) num1);
		stack.push((T) num2);
		stack.push((T) num3);
		stack.push((T) num4);
		stack.push((T) num5);
	}
	
	public void testIsEmpty()
	{
		setupEscenario1();
		assertEquals("La lista deberìa estar vacìa",  true , stack.isEmpty());
		setupEscenario2();
		assertEquals("La lista no deberìa estar vacìa",  false , stack.isEmpty());
	}
	
	public void testGetSize()
	{
		setupEscenario1();
		assertEquals("El tamaño deberìa ser 0", 0, stack.getSize().intValue());
		setupEscenario2();
		assertEquals("El tamaño deberìa ser 5", 5, stack.getSize().intValue());
	}
	
	public void testTop()
	{
		setupEscenario2();
		Integer num = new Integer(49);
		T actual = stack.top();
		assertEquals("El ultimo nùmero no es el adecuado", 50, actual);
		stack.push((T) num);
		actual = stack.top();
		assertEquals("El ultimo nùmero no es el adecuado", num, actual);
		
	}
	
	public void testPush()
	{
		setupEscenario1();
		Integer num1 = new Integer(200);
		Integer num2= new Integer(300);
		assertEquals("Deberìa ser 0", 0, stack.getSize().intValue());
		stack.push((T) num1);
		assertEquals("Deberìa ser 1", 1, stack.getSize().intValue());
		stack.push((T) num2);
		assertEquals("Deberìa ser 2", 2, stack.getSize().intValue());
		
		setupEscenario2();
		assertEquals("Deberìa ser 5", 5, stack.getSize().intValue());
		stack.push((T) num1);
		assertEquals("Deberìa ser 6", 6, stack.getSize().intValue());
		stack.push((T) num2);
		assertEquals("Deberìa ser 7", 7, stack.getSize().intValue());
	}
	
	public void testPop()
	{
		setupEscenario1();
		Integer num1 = new Integer(10);
		Integer num2 = new Integer(20);
		Integer num3 = new Integer(30);
		Integer num4 = new Integer(40);
		Integer num5 = new Integer(50);
		stack.push((T) num1);
		stack.push((T) num2);
		stack.push((T) num3);
		stack.push((T) num4);
		stack.push((T) num5);
		
		T del1= stack.pop();
		assertEquals("El dato eliminado no es el adecuado", num5, del1);
		assertEquals("Deberìa haber menos elementos", 4, stack.getSize().intValue());
		
		del1= stack.pop();
		assertEquals("El dato eliminado no es el adecuado", num4, del1);
		assertEquals("Deberìa haber menos elementos", 3, stack.getSize().intValue());
		
		
	}
}
