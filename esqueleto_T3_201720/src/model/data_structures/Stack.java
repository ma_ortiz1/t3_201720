package model.data_structures;

public class Stack<E> implements IStack<E>{

	private Node<E> first;
	private int size;
	
	private class Node<E>
	{
		E e;
		Node<E> next;
	}
	public Stack()
	{
		size = 0;
		first = null;
		
	}
	
	
	@Override
	public void push(E e) {
		// TODO Auto-generated method stub
		Node<E> oldFirst= first;
		Node<E> newNode = new Node<E>();
		newNode.e = e;
		newNode.next = oldFirst;
		first = newNode;
		size++;
		
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		E rta = first.e;
		first= first.next;
		size--;
		return rta;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return new Integer(size);
	}

	@Override
	public E top() {
		// TODO Auto-generated method stub
		return first != null? first.e: null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0;
	}

}
